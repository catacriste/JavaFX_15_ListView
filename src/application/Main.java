package application;
	
import java.util.Observable;


import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.BorderPane;
/*ListView - � uma lista de objetos baseados numa collection
 * 
 * Passo 1 - criar a ListView collection, de Strings
 * Passo 2 - adicionar-lhe v�rias strings com as op��es pretendidas
 * Passo 3 - criar um listener para tratar da escola
 * 
 * Copiar a classe Main do exerc�cio anterior
 * Nota : Vamos precisar da alertBox de Utils*/
import javafx.scene.layout.VBox;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
		
			
			ListView<String> listView = new ListView<>();
			
			listView.getItems().add("Op��o 1");
			listView.getItems().add("Op��o 2");
			listView.getItems().add("Op��o 3");
			listView.getItems().addAll("Op��o 4", "Op��o 5","Op��o 6","Op��o 7","Op��o 8","Op��o 9");
			
			listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);			//M�too de sele��o dos itensa
			
			//Listener - capta uma altera��o na sele��o~
			//1� Bot�o
			Button btnLista1 = new Button("Sele��o");
			btnLista1.setOnAction(e -> {
				String msg = "";														//String inicializada sozinha
				for(String m : listView.getSelectionModel().getSelectedItems()) {			//Recolhe todos os itens selecionados
					msg = msg + m+"\n";
					
				}
				Utils.alertBox("Sele��o", "Op��o Selecionada:\n"+ msg);
			});
			
			// 2� bot�o mostra a utiliza��o do Objeto ObservableList
			Button btnLista2 = new Button("Sele��o");
			btnLista2.setOnAction(e -> {
				String msg = "";															//String inicializa��o vazia
				ObservableList<String> listaSele��o;										//Lista de Objetos do tipo String, que o foreach papa
				listaSele��o = listView.getSelectionModel().getSelectedItems();				//Recole todos os itens selecionados
				for(String m : listaSele��o){												//Foreach s� itera ObservableLists
					msg = msg + m+"\n";
				}
				Utils.alertBox("Sele��o", "Op��o Selecionada:\n"+msg);
			});
			
			/*- getSelectionModel - define o m�todo de sele��o dos itens, h� varios :
			 * 		- selectNext. selectPrevious, selectedIndexProprety, etc
			 * 		- selectedItemProprety - permite apenas uma sele��o por objeto
			 * - addListener ativa o detetor de eventos para altera��es no itema
			 * 	-express�o LAMBDA com 3 parametros entrada, dados por selectedItemProperty(v, oldValue , newValue)
			 * 			-v � a lista de op��es
			 * 			-oldValue � item que estava selecionado
			 * 			- newValue � o novo item selecionado*/
			
			
			
			VBox layoutListView = new VBox(10);
			layoutListView.setPadding(new Insets(20,20,20,20));
			layoutListView.getChildren().addAll(listView, btnLista1,btnLista2);
			
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layoutListView,300,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}